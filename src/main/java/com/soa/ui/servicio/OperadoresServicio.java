package com.soa.ui.servicio;



import com.soa.ui.application.model.ObtenerOperador;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Repository
@FeignClient(name = "rrhh-api", url = "${rrhh-api.url}")
public interface OperadoresServicio {

    @GetMapping(value = "/operador", consumes = "application/json")
    ResponseEntity<List<ObtenerOperador>> consultarOperadores(
            @RequestParam(value="parametro") String parametro,
            @SpringQueryMap ObtenerOperador parametros
    );

    @GetMapping("/operador/{operadorId}")
    ResponseEntity<ObtenerOperador> obtenerOperador(
            @PathVariable("operadorId") Integer operadorId
    );
}
