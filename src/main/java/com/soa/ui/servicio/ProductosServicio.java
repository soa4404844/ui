package com.soa.ui.servicio;


import com.soa.ui.application.model.ObtenerProducto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Repository
@FeignClient(name = "productos-api", url = "${productos-api.url}")
public interface ProductosServicio {

    @GetMapping("/producto")
    ResponseEntity<List<ObtenerProducto>> consultarProductos(
            @RequestParam(value="parametro")  String parametro,
            @SpringQueryMap ObtenerProducto parametros
    );

    @GetMapping("/producto/{productoId}")
    ResponseEntity<ObtenerProducto> obtenerProducto(
            @PathVariable("productoId") Integer productoId
    );

    @GetMapping("/producto/{productoId}/stock")
    ResponseEntity<ObtenerProducto> verificarStockProducto(
            @PathVariable("productoId") Integer productoId,
            @RequestParam("cantidad") Double cantidad
    );

}
