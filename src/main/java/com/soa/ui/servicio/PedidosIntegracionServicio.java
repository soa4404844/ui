package com.soa.ui.servicio;

import com.soa.ui.application.model.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Repository
@FeignClient(name = "pedidos-integracion-api", url = "${pedidos-integracion-api.url}")
public interface PedidosIntegracionServicio {

    @PostMapping(value = "/integracion/pedido", consumes = "application/json")
    ResponseEntity<RegistrarPedido> registrarPedido(
            @RequestBody RegistrarPedido registrarPedido
    );


    @GetMapping("/integracion/pedido")
    ResponseEntity<List<ConsultarPedido>> consultarPedidos(
            @RequestParam(value="parametro")  String parametro,
            @SpringQueryMap ObtenerPedido parametros
    );


    @PostMapping("/integracion/pedido/{pedidoId}/asignar")
    ResponseEntity<AsignarPedido> asignarPedido(
            @PathVariable("pedidoId") Integer pedidoId,
            @RequestBody AsignarPedido asignarPedido
    );

    @PutMapping("/integracion/pedido/{pedidoId}/atender")
    ResponseEntity<AtenderPedido> atenderPedido(
            @PathVariable("pedidoId") Integer pedidoId
    );


}
