package com.soa.ui;

import com.soa.ui.vista.MainVista;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.awt.*;

@SpringBootApplication
@ComponentScan("com.soa.ui.*")
@EnableFeignClients(basePackages = "com.soa.ui.servicio")
public class UiApplication {

	@Autowired
	private MainVista mainVista;


	public static void main(String[] args) {
		new SpringApplicationBuilder(UiApplication.class)
				.headless(false)
				.run(args);
	}


	@Bean
	public CommandLineRunner commandLineRunner() {
		return args -> {
			EventQueue.invokeLater(() -> {
				mainVista.initComponents();
				mainVista.setVisible(true);
			});
		};
	}
}
