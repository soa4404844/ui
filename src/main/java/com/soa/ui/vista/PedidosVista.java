package com.soa.ui.vista;


import com.soa.ui.application.model.*;
import com.soa.ui.servicio.OperadoresServicio;
import com.soa.ui.servicio.PedidosIntegracionServicio;
import com.soa.ui.servicio.ProductosServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

@Component
public class PedidosVista extends JTabbedPane {


    private final Function<OffsetDateTime, String> formatOffsetDateTimaToString = (value) -> value != null
            ? DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
            .withZone(ZoneId.systemDefault())
            .format(value)
            : "";

    private final Function<LocalDate, String> formatLocalDateToString = (value) -> value != null
            ? DateTimeFormatter.ofPattern("yyyy-MM-dd")
            .withZone(ZoneId.systemDefault())
            .format(value)
            : "";
    private final Function<String, LocalDate> formatStringToLocalDate = (value) -> LocalDateTime.parse(
            value,
            DateTimeFormatter.ofPattern("yyyy-MM-dd")
    ).atZone(ZoneId.systemDefault()).toLocalDate();

    private final  Function<String, Double> formatDouble = (str) -> {
        var doubleValido = Double.parseDouble(str);

        if (doubleValido < 0){
            throw new NumberFormatException();
        }
        return doubleValido;
    };

    @Autowired
    private PedidosIntegracionServicio pedidosIntegracionServicio;

    @Autowired
    private ProductosServicio productosServicio;

    @Autowired
    private OperadoresServicio operadoresServicio;

    private javax.swing.JTabbedPane compraTabbedPane;
    private javax.swing.JComboBox<String> consultEstadoComboBox;
    private javax.swing.JLabel consultaBuscarLabel;
    private javax.swing.JTextField consultaBuscarTextField;
    private javax.swing.JLabel consultaEstadojLabel;
    private javax.swing.JPanel consultaPanel;
    private javax.swing.JTable pedidoTable;
    private javax.swing.JScrollPane pedidosTablejScrollPane;
    private javax.swing.JButton nuevoPedidoButton;
    private javax.swing.JButton detalleVerPedidoButton;


    private Boolean detalleInProgress = false;

    private Integer selectedRegistroTable;



    private javax.swing.JPanel registroPanel;

    private javax.swing.JButton nuevoCancelarButton;
    private javax.swing.JButton nuevoContactoAgregarjButton;
    private javax.swing.JLabel nuevoContactojLabel;
    private javax.swing.JTextField nuevoContactojTextField;
    private javax.swing.JComboBox<String> nuevoEstadoJComboBox;
    private javax.swing.JLabel nuevoEstadoLabel;
    private javax.swing.JTextField nuevoFechaEntregaTextField;
    private javax.swing.JLabel nuevoFechaEntregajLabel;
    private javax.swing.JLabel nuevoNumeroVentaLabel;
    private javax.swing.JTextField nuevoNumeroVentaTextField;
    private javax.swing.JLabel nuevoPedidoLabel;
    private javax.swing.JButton nuevoProductoAgregarjButton;
    private javax.swing.JButton nuevoProductoEliminarjButton;
    private javax.swing.JLabel nuevoProductosjLabel;
    private javax.swing.JScrollPane nuevoProductosjScrollPane;
    private javax.swing.JTable nuevoProductosjTable;
    private List<ObtenerProducto> nuevoPedidosProductos;
    private javax.swing.JButton nuevoRegistrarButton;
    private javax.swing.JLabel nuevoValidacionLabel;

    private JDialog nuevoContactoRegistrarDialog;
    private javax.swing.JLabel nuevoContactoApellidoLabel;
    private javax.swing.JTextField nuevoContactoApellidoTextField;
    private javax.swing.JLabel nuevoContactoCorreoLabel;
    private javax.swing.JTextField nuevoContactoCorreoTextField;
    private javax.swing.JLabel nuevoContactoDireccionLabel;
    private javax.swing.JTextField nuevoContactoDireccionTextField;
    private javax.swing.JLabel nuevoContactoNombreLabel;
    private javax.swing.JTextField nuevoContactoNombreTextField;
    private javax.swing.JLabel nuevoContactoNumDocLabel;
    private javax.swing.JTextField nuevoContactoNumDocTextField;
    private javax.swing.JPanel nuevoContactoPanel;
    private javax.swing.JButton nuevoContactoRegistrarButton;
    private javax.swing.JTextField nuevoContactoTIpoDocTextField;
    private javax.swing.JLabel nuevoContactoTelefonoLabel;
    private javax.swing.JTextField nuevoContactoTelefonoTextField;
    private javax.swing.JLabel nuevoContactoTipoDocLabel;
    private javax.swing.JLabel nuevoContactoTituloLabel;
    private javax.swing.JLabel nuevoContactoValidacionLabel;
    private GuardarContacto guardarContacto;

    private JDialog seleccionProductoDialog;
    private javax.swing.JLabel seleccionBuscarProductoLabel;
    private javax.swing.JTextField seleccionBuscarProductoTextField;
    private javax.swing.JLabel seleccionCantidadLabel;
    private javax.swing.JTextField seleccionCantidadTextField;
    private javax.swing.JList<ObtenerProducto> seleccionProductoList;
    private javax.swing.JButton seleccionProductoOkButton;
    private javax.swing.JPanel seleccionProductoPanel;
    private javax.swing.JLabel seleccionProductoTituloLabel;
    private javax.swing.JLabel seleccionProductoValidacionLabel;
    private javax.swing.JScrollPane seleccionProductojScrollPane;
    private Integer nuevoSelectedProductoTable;
    private ObtenerProducto productoSeleccionado;



    private javax.swing.JLabel detalleContactoLabel;
    private javax.swing.JTextField detalleContactoTextField;
    private javax.swing.JButton detalleAtenderButton;
    private javax.swing.JComboBox<String> detalleEstadoComboBox;
    private javax.swing.JLabel detalleEstadoLabel;
    private javax.swing.JLabel detalleFechaEntregaLabel;
    private javax.swing.JTextField detalleFechaEntregaTextField;
    private javax.swing.JLabel detalleNumeroVentaLabel;
    private javax.swing.JTextField detalleNumeroVentaTextField;
    private javax.swing.JLabel detalleOperadorLabel;
    private javax.swing.JTextField detalleOperadorTextField;
    private javax.swing.JPanel detallePanel;
    private javax.swing.JLabel detallePedidoLabel;
    private javax.swing.JLabel detalleProductosjLabel;
    private javax.swing.JScrollPane detalleProductosjScrollPane;
    private javax.swing.JTable detalleProductosjTable;
    private javax.swing.JButton detalleSalirButton;
    private javax.swing.JLabel detalleValidacionLabel;
    private javax.swing.JButton detalleVerContactojButton;
    private javax.swing.JButton detalleBuscarOperadorjButton;
    private ConsultarPedido detallePedido;


    private JDialog detalleContactoDialog;
    private javax.swing.JLabel detalleContactoApellidoLabel;
    private javax.swing.JTextField detalleContactoApellidoTextField;
    private javax.swing.JLabel detalleContactoCorreoLabel;
    private javax.swing.JTextField detalleContactoCorreoTextField;
    private javax.swing.JLabel detalleContactoDireccionLabel;
    private javax.swing.JTextField detalleContactoDireccionTextField;
    private javax.swing.JLabel detalleContactoNombreLabel;
    private javax.swing.JTextField detalleContactoNombreTextField;
    private javax.swing.JLabel detalleContactoNumDocLabel;
    private javax.swing.JTextField detalleContactoNumDocTextField;
    private javax.swing.JPanel detalleContactoPanel;
    private javax.swing.JTextField detalleContactoTIpoDocTextField;
    private javax.swing.JLabel detalleContactoTelefonoLabel;
    private javax.swing.JTextField detalleContactoTelefonoTextField;
    private javax.swing.JLabel detalleContactoTipoDocLabel;
    private javax.swing.JLabel detalleContactoTituloLabel;



    private JDialog detalleOperadorDialog;
    private ObtenerOperador operadorSeleccionado;
    private javax.swing.JLabel buscarOperadorLabel;
    private javax.swing.JList<ObtenerOperador> buscarOperadorList;
    private javax.swing.JButton buscarOperadorOkButton;
    private javax.swing.JPanel buscarOperadorPanel;
    private javax.swing.JTextField buscarOperadorTextField;
    private javax.swing.JLabel buscarOperadorTituloLabel;
    private javax.swing.JScrollPane buscarOperadorjScrollPane;
    private javax.swing.JLabel buscarOperadorValidacionLabel;


    public void initComponents() {

        compraTabbedPane = new javax.swing.JTabbedPane();
        consultaPanel = new javax.swing.JPanel();
        consultaBuscarLabel = new javax.swing.JLabel();
        consultaBuscarTextField = new javax.swing.JTextField();
        nuevoPedidoButton = new javax.swing.JButton();
        pedidosTablejScrollPane = new javax.swing.JScrollPane();
        pedidoTable = new javax.swing.JTable();
        detalleVerPedidoButton = new javax.swing.JButton();
        consultEstadoComboBox = new javax.swing.JComboBox<>();
        consultaEstadojLabel = new javax.swing.JLabel();


        registroPanel = new javax.swing.JPanel();
        nuevoPedidoLabel = new javax.swing.JLabel();
        nuevoNumeroVentaTextField = new javax.swing.JTextField();
        nuevoNumeroVentaLabel = new javax.swing.JLabel();
        nuevoRegistrarButton = new javax.swing.JButton();
        nuevoCancelarButton = new javax.swing.JButton();
        nuevoValidacionLabel = new javax.swing.JLabel();
        nuevoFechaEntregaTextField = new javax.swing.JTextField();
        nuevoFechaEntregajLabel = new javax.swing.JLabel();
        nuevoEstadoLabel = new javax.swing.JLabel();
        nuevoEstadoJComboBox = new javax.swing.JComboBox<>();
        nuevoProductosjLabel = new javax.swing.JLabel();
        nuevoProductosjScrollPane = new javax.swing.JScrollPane();
        nuevoProductosjTable = new javax.swing.JTable();
        nuevoProductoAgregarjButton = new javax.swing.JButton();
        nuevoProductoEliminarjButton = new javax.swing.JButton();
        nuevoContactojLabel = new javax.swing.JLabel();
        nuevoContactojTextField = new javax.swing.JTextField();
        nuevoContactoAgregarjButton = new javax.swing.JButton();


        nuevoContactoPanel = new javax.swing.JPanel();
        nuevoContactoTituloLabel = new javax.swing.JLabel();
        nuevoContactoNombreLabel = new javax.swing.JLabel();
        nuevoContactoNombreTextField = new javax.swing.JTextField();
        nuevoContactoRegistrarButton = new javax.swing.JButton();
        nuevoContactoValidacionLabel = new javax.swing.JLabel();
        nuevoContactoApellidoLabel = new javax.swing.JLabel();
        nuevoContactoApellidoTextField = new javax.swing.JTextField();
        nuevoContactoNumDocLabel = new javax.swing.JLabel();
        nuevoContactoNumDocTextField = new javax.swing.JTextField();
        nuevoContactoTipoDocLabel = new javax.swing.JLabel();
        nuevoContactoTIpoDocTextField = new javax.swing.JTextField();
        nuevoContactoTelefonoLabel = new javax.swing.JLabel();
        nuevoContactoTelefonoTextField = new javax.swing.JTextField();
        nuevoContactoCorreoLabel = new javax.swing.JLabel();
        nuevoContactoCorreoTextField = new javax.swing.JTextField();
        nuevoContactoDireccionLabel = new javax.swing.JLabel();
        nuevoContactoDireccionTextField = new javax.swing.JTextField();

        seleccionProductoPanel = new javax.swing.JPanel();
        seleccionProductoTituloLabel = new javax.swing.JLabel();
        seleccionBuscarProductoLabel = new javax.swing.JLabel();
        seleccionBuscarProductoTextField = new javax.swing.JTextField();
        seleccionProductoOkButton = new javax.swing.JButton();
        seleccionProductojScrollPane = new javax.swing.JScrollPane();
        seleccionProductoList = new javax.swing.JList<>();
        seleccionCantidadTextField = new javax.swing.JTextField();
        seleccionCantidadLabel = new javax.swing.JLabel();
        seleccionProductoValidacionLabel = new javax.swing.JLabel();


        detallePanel = new javax.swing.JPanel();
        detallePedidoLabel = new javax.swing.JLabel();
        detalleNumeroVentaLabel = new javax.swing.JLabel();
        detalleNumeroVentaTextField = new javax.swing.JTextField();
        detalleSalirButton = new javax.swing.JButton();
        detalleFechaEntregaLabel = new javax.swing.JLabel();
        detalleContactoTextField = new javax.swing.JTextField();
        detalleEstadoComboBox = new javax.swing.JComboBox<>();
        detalleEstadoLabel = new javax.swing.JLabel();
        detalleProductosjLabel = new javax.swing.JLabel();
        detalleProductosjScrollPane = new javax.swing.JScrollPane();
        detalleProductosjTable = new javax.swing.JTable();
        detalleContactoLabel = new javax.swing.JLabel();
        detalleFechaEntregaTextField = new javax.swing.JTextField();
        detalleVerContactojButton = new javax.swing.JButton();
        detalleAtenderButton = new javax.swing.JButton();
        detalleValidacionLabel = new javax.swing.JLabel();
        detalleOperadorLabel = new javax.swing.JLabel();
        detalleOperadorTextField = new javax.swing.JTextField();
        detalleBuscarOperadorjButton = new javax.swing.JButton();


        detalleContactoPanel = new javax.swing.JPanel();
        detalleContactoTituloLabel = new javax.swing.JLabel();
        detalleContactoNombreLabel = new javax.swing.JLabel();
        detalleContactoNombreTextField = new javax.swing.JTextField();
        detalleContactoApellidoLabel = new javax.swing.JLabel();
        detalleContactoApellidoTextField = new javax.swing.JTextField();
        detalleContactoNumDocLabel = new javax.swing.JLabel();
        detalleContactoNumDocTextField = new javax.swing.JTextField();
        detalleContactoTipoDocLabel = new javax.swing.JLabel();
        detalleContactoTIpoDocTextField = new javax.swing.JTextField();
        detalleContactoTelefonoLabel = new javax.swing.JLabel();
        detalleContactoTelefonoTextField = new javax.swing.JTextField();
        detalleContactoCorreoLabel = new javax.swing.JLabel();
        detalleContactoCorreoTextField = new javax.swing.JTextField();
        detalleContactoDireccionLabel = new javax.swing.JLabel();
        detalleContactoDireccionTextField = new javax.swing.JTextField();


        buscarOperadorPanel = new javax.swing.JPanel();
        buscarOperadorTituloLabel = new javax.swing.JLabel();
        buscarOperadorLabel = new javax.swing.JLabel();
        buscarOperadorTextField = new javax.swing.JTextField();
        buscarOperadorOkButton = new javax.swing.JButton();
        buscarOperadorjScrollPane = new javax.swing.JScrollPane();
        buscarOperadorList = new javax.swing.JList<>();
        buscarOperadorValidacionLabel = new javax.swing.JLabel();

        setMinimumSize(new Dimension(800, 640));
        setPreferredSize(new Dimension(880, 640));


        consultaBuscarLabel.setText("Buscar :");

        consultaBuscarTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void removeUpdate(DocumentEvent e) {
                consultaBuscarTextFieldActionPerformed();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                consultaBuscarTextFieldActionPerformed();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });

        nuevoPedidoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/anadir.png"))); // NOI18N
        nuevoPedidoButton.setText("Nuevo");
        nuevoPedidoButton.addActionListener(this::nuevoPedidoButtonActionPerformed);

        pedidoTable.setModel(new PedidosTableModel(
                pedidosIntegracionServicio.consultarPedidos(null, ObtenerPedido.builder().build()).getBody()
        ));
        pedidoTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        pedidoTable.setRowSelectionAllowed(true);
        pedidoTable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (!detalleInProgress) {
                    int row = pedidoTable.getSelectedRow();

                    if (selectedRegistroTable != null && selectedRegistroTable.equals(row)) {
                        restablecerTablaSeleccion();
                    } else {
                        selectedRegistroTable = row;
                        detalleVerPedidoButton.setText("Pedido N°: " + pedidoTable.getValueAt(row, 0));
                        detalleVerPedidoButton.setVisible(true);
                    }
                }
            }
        });

        pedidosTablejScrollPane.setViewportView(pedidoTable);

        detalleVerPedidoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/lupa.png"))); // NOI18N
        detalleVerPedidoButton.setText("Pedido N°");
        detalleVerPedidoButton.addActionListener(this::detalleVerPedidoButtonActionPerformed);
        detalleVerPedidoButton.setVisible(false);

        consultEstadoComboBox.setModel(new DefaultComboBoxModel<>(
                Arrays.stream(PedidoEstado.values())
                        .map(PedidoEstado::getDescripcion)
                        .toArray(String[]::new)
        ));
        consultEstadoComboBox.addActionListener(this::consultaEstadojComboBoxActionPerformed);

        consultaEstadojLabel.setText("Estado:");

        javax.swing.GroupLayout consultaPanelLayout = new javax.swing.GroupLayout(consultaPanel);
        consultaPanel.setLayout(consultaPanelLayout);
        consultaPanelLayout.setHorizontalGroup(
                consultaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, consultaPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(consultaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(pedidosTablejScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 868, Short.MAX_VALUE)
                                        .addGroup(consultaPanelLayout.createSequentialGroup()
                                                .addComponent(consultaBuscarLabel)
                                                .addGap(18, 18, 18)
                                                .addComponent(consultaBuscarTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(consultaEstadojLabel)
                                                .addGap(18, 18, 18)
                                                .addComponent(consultEstadoComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(detalleVerPedidoButton)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(nuevoPedidoButton)))
                                .addContainerGap())
        );
        consultaPanelLayout.setVerticalGroup(
                consultaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(consultaPanelLayout.createSequentialGroup()
                                .addGroup(consultaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(consultaPanelLayout.createSequentialGroup()
                                                .addGap(25, 25, 25)
                                                .addGroup(consultaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(consultaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                .addComponent(consultaEstadojLabel)
                                                                .addComponent(consultEstadoComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(consultaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                .addComponent(consultaBuscarTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(consultaBuscarLabel))))
                                        .addGroup(consultaPanelLayout.createSequentialGroup()
                                                .addContainerGap()
                                                .addGroup(consultaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(nuevoPedidoButton)
                                                        .addComponent(detalleVerPedidoButton))))
                                .addGap(18, 18, 18)
                                .addComponent(pedidosTablejScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 534, Short.MAX_VALUE)
                                .addContainerGap())
        );


        nuevoPedidoLabel.setText("Nuevo pedido:");

        nuevoNumeroVentaLabel.setText("N° Venta");

        nuevoRegistrarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/registrar.png"))); // NOI18N
        nuevoRegistrarButton.setText("Registrar");
        nuevoRegistrarButton.addActionListener(this::nuevoRegistrarButtonActionPerformed);

        nuevoCancelarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/boton-x.png"))); // NOI18N
        nuevoCancelarButton.setText("Cancelar");
        nuevoCancelarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nuevoCancelarButtonActionPerformed(evt);
            }
        });

        nuevoValidacionLabel.setBackground(new java.awt.Color(255, 0, 0));
        nuevoValidacionLabel.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        nuevoValidacionLabel.setForeground(new java.awt.Color(255, 0, 0));
        nuevoValidacionLabel.setText("");

        nuevoFechaEntregajLabel.setText("Fecha Entrega:");
        nuevoFechaEntregaTextField.setText(
                formatLocalDateToString.apply(LocalDate.now())
        );

        nuevoEstadoLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        nuevoEstadoLabel.setText("Estado:");

        nuevoEstadoJComboBox.setModel(new DefaultComboBoxModel<>(
                Arrays.stream(PedidoEstado.values())
                        .map(PedidoEstado::getDescripcion)
                        .filter(pe->!pe.isEmpty())
                        .toArray(String[]::new)
        ));

        nuevoEstadoJComboBox.setEnabled(false);
        nuevoEstadoJComboBox.setEditable(false);

        nuevoProductosjLabel.setText("Productos Pedidos:");

        nuevoPedidosProductos = new ArrayList<>();
        nuevoProductosjTable.setModel(new PedidoProductoTableModel(new ArrayList<>()));
        nuevoProductosjTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        nuevoProductosjTable.setRowSelectionAllowed(true);
        nuevoProductosjTable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                nuevoProductoTablaSeleccionMouseClicked();
            }
        });
        nuevoProductosjScrollPane.setViewportView(nuevoProductosjTable);

        nuevoProductoAgregarjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/anadir_16px.png"))); // NOI18N
        nuevoProductoAgregarjButton.setToolTipText("Agregar Productos");
        nuevoProductoAgregarjButton.addActionListener(this::nuevoProductoAgregarjButtonActionPerformed);

        nuevoProductoEliminarjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/eliminar_16px.png"))); // NOI18N
        nuevoProductoEliminarjButton.setToolTipText("Eliminar Producto");
        nuevoProductoEliminarjButton.addActionListener(this::nuevoProductoEliminarjButtonActionPerformed);
        nuevoProductoEliminarjButton.setEnabled(false);

        nuevoContactojLabel.setText("Contacto:");

        nuevoContactojTextField.setPreferredSize(new java.awt.Dimension(64, 23));
        nuevoContactojTextField.setEditable(false);

        nuevoContactoAgregarjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/anadir_16px.png"))); // NOI18N
        nuevoContactoAgregarjButton.setToolTipText("Agregar Contacto");
        nuevoContactoAgregarjButton.setMaximumSize(new java.awt.Dimension(23, 23));
        nuevoContactoAgregarjButton.setMinimumSize(new java.awt.Dimension(23, 23));
        nuevoContactoAgregarjButton.setPreferredSize(new java.awt.Dimension(23, 23));
        nuevoContactoAgregarjButton.addActionListener(this::nuevoContactoAgregarjButtonActionPerformed);

        javax.swing.GroupLayout registroPanelLayout = new javax.swing.GroupLayout(registroPanel);
        registroPanel.setLayout(registroPanelLayout);
        registroPanelLayout.setHorizontalGroup(
                registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, registroPanelLayout.createSequentialGroup()
                                .addGap(15, 15, 15)
                                .addGroup(registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(registroPanelLayout.createSequentialGroup()
                                                .addComponent(nuevoPedidoLabel)
                                                .addGap(18, 18, 18)
                                                .addComponent(nuevoValidacionLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, registroPanelLayout.createSequentialGroup()
                                                .addGroup(registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(registroPanelLayout.createSequentialGroup()
                                                                .addGap(282, 282, 282)
                                                                .addComponent(nuevoCancelarButton))
                                                        .addGroup(registroPanelLayout.createSequentialGroup()
                                                                .addGroup(registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(nuevoNumeroVentaLabel)
                                                                        .addComponent(nuevoEstadoLabel))
                                                                .addGap(37, 37, 37)
                                                                .addGroup(registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(nuevoEstadoJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(nuevoNumeroVentaTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                                .addGap(38, 38, 38)
                                                .addGroup(registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(registroPanelLayout.createSequentialGroup()
                                                                .addComponent(nuevoRegistrarButton)
                                                                .addGap(0, 0, Short.MAX_VALUE))
                                                        .addGroup(registroPanelLayout.createSequentialGroup()
                                                                .addGroup(registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(nuevoFechaEntregajLabel)
                                                                        .addComponent(nuevoContactojLabel))
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addGroup(registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(nuevoFechaEntregaTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, registroPanelLayout.createSequentialGroup()
                                                                                .addComponent(nuevoContactojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                .addComponent(nuevoContactoAgregarjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, registroPanelLayout.createSequentialGroup()
                                                .addGap(0, 2, Short.MAX_VALUE)
                                                .addGroup(registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addComponent(nuevoProductosjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 844, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(registroPanelLayout.createSequentialGroup()
                                                                .addComponent(nuevoProductosjLabel)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(nuevoProductoEliminarjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(nuevoProductoAgregarjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addGap(19, 19, 19))
        );
        registroPanelLayout.setVerticalGroup(
                registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(registroPanelLayout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(nuevoPedidoLabel)
                                        .addComponent(nuevoValidacionLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(46, 46, 46)
                                .addGroup(registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(registroPanelLayout.createSequentialGroup()
                                                .addGroup(registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(nuevoFechaEntregajLabel)
                                                        .addComponent(nuevoFechaEntregaTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(41, 41, 41)
                                                .addGroup(registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(nuevoContactojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(nuevoContactoAgregarjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(registroPanelLayout.createSequentialGroup()
                                                .addGroup(registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(nuevoNumeroVentaTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(nuevoNumeroVentaLabel))
                                                .addGap(41, 41, 41)
                                                .addGroup(registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(nuevoEstadoLabel)
                                                        .addComponent(nuevoEstadoJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(nuevoContactojLabel))))
                                .addGap(46, 46, 46)
                                .addGroup(registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(nuevoProductoEliminarjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(nuevoProductoAgregarjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(nuevoProductosjLabel, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addGap(18, 18, 18)
                                .addComponent(nuevoProductosjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
                                .addGroup(registroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(nuevoRegistrarButton)
                                        .addComponent(nuevoCancelarButton))
                                .addGap(21, 21, 21))
        );

        nuevoContactoPanel.setPreferredSize(new java.awt.Dimension(420, 400));
        nuevoContactoPanel.setVerifyInputWhenFocusTarget(false);

        nuevoContactoTituloLabel.setText("Nuevo Contacto");

        nuevoContactoNombreLabel.setText("Nombre:");

        nuevoContactoRegistrarButton.setText("Registrar");
        nuevoContactoRegistrarButton.addActionListener(this::nuevoContactoRegistrarButtonActionPerformed);

        nuevoContactoValidacionLabel.setBackground(new java.awt.Color(255, 0, 0));
        nuevoContactoValidacionLabel.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        nuevoContactoValidacionLabel.setForeground(new java.awt.Color(255, 0, 0));
        nuevoContactoValidacionLabel.setText("");

        nuevoContactoApellidoLabel.setText("Apellidos:");
        nuevoContactoNumDocLabel.setText("N Doc:");
        nuevoContactoTipoDocLabel.setText("Tipo Doc:");
        nuevoContactoTelefonoLabel.setText("Telefono:");
        nuevoContactoCorreoLabel.setText("Correo:");
        nuevoContactoDireccionLabel.setText("Dirección:");

        javax.swing.GroupLayout nuevoContactoPanelLayout = new javax.swing.GroupLayout(nuevoContactoPanel);
        nuevoContactoPanel.setLayout(nuevoContactoPanelLayout);
        nuevoContactoPanelLayout.setHorizontalGroup(
                nuevoContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(nuevoContactoPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(nuevoContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, nuevoContactoPanelLayout.createSequentialGroup()
                                                .addComponent(nuevoContactoValidacionLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 326, Short.MAX_VALUE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(nuevoContactoRegistrarButton))
                                        .addGroup(nuevoContactoPanelLayout.createSequentialGroup()
                                                .addComponent(nuevoContactoTituloLabel)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                        .addGroup(nuevoContactoPanelLayout.createSequentialGroup()
                                                .addGroup(nuevoContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(nuevoContactoApellidoLabel)
                                                        .addComponent(nuevoContactoNombreLabel)
                                                        .addComponent(nuevoContactoTipoDocLabel)
                                                        .addComponent(nuevoContactoNumDocLabel)
                                                        .addComponent(nuevoContactoTelefonoLabel)
                                                        .addComponent(nuevoContactoCorreoLabel))
                                                .addGap(13, 13, 13)
                                                .addGroup(nuevoContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(nuevoContactoCorreoTextField)
                                                        .addComponent(nuevoContactoTelefonoTextField)
                                                        .addComponent(nuevoContactoNumDocTextField)
                                                        .addComponent(nuevoContactoTIpoDocTextField)
                                                        .addComponent(nuevoContactoApellidoTextField)
                                                        .addComponent(nuevoContactoNombreTextField)))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, nuevoContactoPanelLayout.createSequentialGroup()
                                                .addComponent(nuevoContactoDireccionLabel)
                                                .addGap(13, 13, 13)
                                                .addComponent(nuevoContactoDireccionTextField)))
                                .addContainerGap())
        );
        nuevoContactoPanelLayout.setVerticalGroup(
                nuevoContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(nuevoContactoPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(nuevoContactoTituloLabel)
                                .addGap(31, 31, 31)
                                .addGroup(nuevoContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(nuevoContactoNombreLabel)
                                        .addComponent(nuevoContactoNombreTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(24, 24, 24)
                                .addGroup(nuevoContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(nuevoContactoApellidoLabel)
                                        .addComponent(nuevoContactoApellidoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(24, 24, 24)
                                .addGroup(nuevoContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(nuevoContactoTipoDocLabel)
                                        .addComponent(nuevoContactoTIpoDocTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(24, 24, 24)
                                .addGroup(nuevoContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(nuevoContactoNumDocLabel)
                                        .addComponent(nuevoContactoNumDocTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(24, 24, 24)
                                .addGroup(nuevoContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(nuevoContactoTelefonoLabel)
                                        .addComponent(nuevoContactoTelefonoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(24, 24, 24)
                                .addGroup(nuevoContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(nuevoContactoCorreoLabel)
                                        .addComponent(nuevoContactoCorreoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(nuevoContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(nuevoContactoDireccionLabel)
                                        .addComponent(nuevoContactoDireccionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                                .addGroup(nuevoContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(nuevoContactoRegistrarButton)
                                        .addComponent(nuevoContactoValidacionLabel))
                                .addContainerGap())
        );


        seleccionProductoPanel.setPreferredSize(new java.awt.Dimension(400, 280));

        seleccionProductoTituloLabel.setText("Seleccione Producto:");

        seleccionBuscarProductoLabel.setText("Buscar:");

        seleccionBuscarProductoTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void removeUpdate(DocumentEvent e) {
                seleccionBuscarProductoTextFieldActionPerformed();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                seleccionBuscarProductoTextFieldActionPerformed();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });

        seleccionProductoOkButton.setText("OK");
        seleccionProductoOkButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                seleccionProductoOkButtonActionPerformed(evt);
            }
        });

        seleccionProductoList.setModel(new ProductoListModel(
                productosServicio.consultarProductos(
                        null,
                        ObtenerProducto.builder().build()
                ).getBody()
        ));
        seleccionProductoList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        seleccionProductoList.setCellRenderer(new ProductoListRenderer());
        seleccionProductoList.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                productoSeleccionado = seleccionProductoList.getSelectedValue();
            }
        });
        seleccionProductojScrollPane.setViewportView(seleccionProductoList);

        seleccionCantidadLabel.setText("Cantidad:");

        seleccionProductoValidacionLabel.setBackground(new java.awt.Color(255, 0, 0));
        seleccionProductoValidacionLabel.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        seleccionProductoValidacionLabel.setForeground(new java.awt.Color(255, 0, 0));
        seleccionProductoValidacionLabel.setText("");

        javax.swing.GroupLayout seleccionProductoPanelLayout = new javax.swing.GroupLayout(seleccionProductoPanel);
        seleccionProductoPanel.setLayout(seleccionProductoPanelLayout);
        seleccionProductoPanelLayout.setHorizontalGroup(
                seleccionProductoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(seleccionProductoPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(seleccionProductoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, seleccionProductoPanelLayout.createSequentialGroup()
                                                .addComponent(seleccionProductoValidacionLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGap(18, 18, 18)
                                                .addComponent(seleccionProductoOkButton))
                                        .addComponent(seleccionProductojScrollPane)
                                        .addGroup(seleccionProductoPanelLayout.createSequentialGroup()
                                                .addComponent(seleccionBuscarProductoLabel)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(seleccionBuscarProductoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 322, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(seleccionProductoPanelLayout.createSequentialGroup()
                                                .addGroup(seleccionProductoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(seleccionProductoTituloLabel)
                                                        .addGroup(seleccionProductoPanelLayout.createSequentialGroup()
                                                                .addComponent(seleccionCantidadLabel)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(seleccionCantidadTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 322, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addGap(0, 0, Short.MAX_VALUE)))
                                .addContainerGap())
        );
        seleccionProductoPanelLayout.setVerticalGroup(
                seleccionProductoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(seleccionProductoPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(seleccionProductoTituloLabel)
                                .addGap(31, 31, 31)
                                .addGroup(seleccionProductoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(seleccionBuscarProductoLabel)
                                        .addComponent(seleccionBuscarProductoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(seleccionProductojScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(seleccionProductoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(seleccionCantidadLabel)
                                        .addComponent(seleccionCantidadTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                                .addGroup(seleccionProductoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(seleccionProductoOkButton)
                                        .addComponent(seleccionProductoValidacionLabel))
                                .addContainerGap())
        );


        detallePedidoLabel.setText("Detalle Pedido: N° ");

        detalleNumeroVentaLabel.setText("N° Venta:");

        detalleSalirButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/salir.png"))); // NOI18N
        detalleSalirButton.setToolTipText("Salir");
        detalleSalirButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                detalleSalirButtonActionPerformed(evt);
            }
        });

        detalleFechaEntregaLabel.setText("Fecha Entrega:");

        detalleEstadoComboBox.setModel(new DefaultComboBoxModel<>(
                Arrays.stream(PedidoEstado.values())
                        .map(PedidoEstado::getDescripcion)
                        .filter(pe->!pe.isEmpty())
                        .toArray(String[]::new)
        ));
        detalleEstadoLabel.setText("Estado:");

        detalleProductosjLabel.setText("Productos Pedido:");

        detalleProductosjTable.setModel(new PedidoProductoTableModel(
                new ArrayList<>()
        ));
        detalleProductosjScrollPane.setViewportView(detalleProductosjTable);

        detalleContactoLabel.setText("Contacto:");

        detalleVerContactojButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/lupa_16px.png"))); // NOI18N
        detalleVerContactojButton.setToolTipText("Buscar");
        detalleVerContactojButton.addActionListener(this::detalleVerContactojButtonActionPerformed);

        detalleAtenderButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/atender.png"))); // NOI18N
        detalleAtenderButton.setToolTipText("Marcar pedido como atendido");
        detalleAtenderButton.setMaximumSize(new java.awt.Dimension(55, 40));
        detalleAtenderButton.setMinimumSize(new java.awt.Dimension(55, 40));
        detalleAtenderButton.setVisible(false);
        detalleAtenderButton.addActionListener(this::detalleAtenderButtonActionPerformed);

        detalleValidacionLabel.setBackground(new java.awt.Color(255, 0, 0));
        detalleValidacionLabel.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        detalleValidacionLabel.setForeground(new java.awt.Color(255, 0, 0));
        detalleValidacionLabel.setText("");

        detalleOperadorLabel.setText("Operador:");

        detalleBuscarOperadorjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/lupa_16px.png"))); // NOI18N
        detalleBuscarOperadorjButton.setToolTipText("Buscar");
        detalleBuscarOperadorjButton.addActionListener(this::detalleBuscarOperadorjButtonActionPerformed);

        javax.swing.GroupLayout detallePanelLayout = new javax.swing.GroupLayout(detallePanel);
        detallePanel.setLayout(detallePanelLayout);
        detallePanelLayout.setHorizontalGroup(
                detallePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(detallePanelLayout.createSequentialGroup()
                                .addGap(15, 15, 15)
                                .addGroup(detallePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(detallePanelLayout.createSequentialGroup()
                                                .addComponent(detalleProductosjLabel)
                                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, detallePanelLayout.createSequentialGroup()
                                                .addGroup(detallePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(detalleProductosjScrollPane, javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(detallePanelLayout.createSequentialGroup()
                                                                .addGroup(detallePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                                        .addGroup(detallePanelLayout.createSequentialGroup()
                                                                                .addComponent(detalleEstadoLabel)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                                .addComponent(detalleEstadoComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                        .addGroup(detallePanelLayout.createSequentialGroup()
                                                                                .addComponent(detalleNumeroVentaLabel)
                                                                                .addGap(55, 55, 55)
                                                                                .addComponent(detalleNumeroVentaTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                                .addGap(80, 80, 80)
                                                                .addGroup(detallePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(detalleFechaEntregaLabel)
                                                                        .addComponent(detalleContactoLabel)
                                                                        .addComponent(detalleOperadorLabel))
                                                                .addGap(18, 18, 18)
                                                                .addGroup(detallePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addGroup(detallePanelLayout.createSequentialGroup()
                                                                                .addComponent(detalleOperadorTextField)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                .addComponent(detalleBuscarOperadorjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                        .addGroup(detallePanelLayout.createSequentialGroup()
                                                                                .addComponent(detalleContactoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                .addComponent(detalleVerContactojButton, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addGap(0, 1, Short.MAX_VALUE))
                                                                        .addComponent(detalleFechaEntregaTextField)))
                                                        .addGroup(detallePanelLayout.createSequentialGroup()
                                                                .addComponent(detallePedidoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(detalleValidacionLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(detalleAtenderButton, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(detalleSalirButton, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addGap(16, 16, 16))))
        );
        detallePanelLayout.setVerticalGroup(
                detallePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(detallePanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(detallePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(detalleSalirButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(detallePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(detallePedidoLabel)
                                                .addComponent(detalleValidacionLabel))
                                        .addComponent(detalleAtenderButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(51, 51, 51)
                                .addGroup(detallePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(detalleNumeroVentaTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(detalleFechaEntregaLabel)
                                        .addComponent(detalleNumeroVentaLabel)
                                        .addComponent(detalleFechaEntregaTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(48, 48, 48)
                                .addGroup(detallePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(detallePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(detalleEstadoLabel)
                                                .addComponent(detalleEstadoComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(detalleContactoLabel)
                                                .addComponent(detalleContactoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(detalleVerContactojButton, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(49, 49, 49)
                                .addGroup(detallePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(detallePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(detalleOperadorLabel)
                                                .addComponent(detalleOperadorTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(detalleBuscarOperadorjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(27, 27, 27)
                                .addComponent(detalleProductosjLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(detalleProductosjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(21, Short.MAX_VALUE))
        );


        detalleContactoPanel.setPreferredSize(new java.awt.Dimension(420, 400));
        detalleContactoPanel.setVerifyInputWhenFocusTarget(false);

        detalleContactoTituloLabel.setText("Contacto: ");

        detalleContactoNombreLabel.setText("Nombre:");

        detalleContactoApellidoLabel.setText("Apellidos:");

        detalleContactoNumDocLabel.setText("N Doc:");

        detalleContactoTipoDocLabel.setText("Tipo Doc:");

        detalleContactoTelefonoLabel.setText("Telefono:");

        detalleContactoCorreoLabel.setText("Correo:");

        detalleContactoDireccionLabel.setText("Dirección:");

        javax.swing.GroupLayout detalleContactoPanelLayout = new javax.swing.GroupLayout(detalleContactoPanel);
        detalleContactoPanel.setLayout(detalleContactoPanelLayout);
        detalleContactoPanelLayout.setHorizontalGroup(
                detalleContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(detalleContactoPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(detalleContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(detalleContactoPanelLayout.createSequentialGroup()
                                                .addComponent(detalleContactoTituloLabel)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                        .addGroup(detalleContactoPanelLayout.createSequentialGroup()
                                                .addGroup(detalleContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(detalleContactoApellidoLabel)
                                                        .addComponent(detalleContactoNombreLabel)
                                                        .addComponent(detalleContactoTipoDocLabel)
                                                        .addComponent(detalleContactoNumDocLabel)
                                                        .addComponent(detalleContactoTelefonoLabel)
                                                        .addComponent(detalleContactoCorreoLabel))
                                                .addGap(13, 13, 13)
                                                .addGroup(detalleContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(detalleContactoCorreoTextField)
                                                        .addComponent(detalleContactoTelefonoTextField)
                                                        .addComponent(detalleContactoNumDocTextField)
                                                        .addComponent(detalleContactoTIpoDocTextField)
                                                        .addComponent(detalleContactoApellidoTextField)
                                                        .addComponent(detalleContactoNombreTextField)))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, detalleContactoPanelLayout.createSequentialGroup()
                                                .addComponent(detalleContactoDireccionLabel)
                                                .addGap(13, 13, 13)
                                                .addComponent(detalleContactoDireccionTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 342, Short.MAX_VALUE)))
                                .addContainerGap())
        );
        detalleContactoPanelLayout.setVerticalGroup(
                detalleContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(detalleContactoPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(detalleContactoTituloLabel)
                                .addGap(31, 31, 31)
                                .addGroup(detalleContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(detalleContactoNombreLabel)
                                        .addComponent(detalleContactoNombreTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(24, 24, 24)
                                .addGroup(detalleContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(detalleContactoApellidoLabel)
                                        .addComponent(detalleContactoApellidoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(24, 24, 24)
                                .addGroup(detalleContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(detalleContactoTipoDocLabel)
                                        .addComponent(detalleContactoTIpoDocTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(24, 24, 24)
                                .addGroup(detalleContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(detalleContactoNumDocLabel)
                                        .addComponent(detalleContactoNumDocTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(24, 24, 24)
                                .addGroup(detalleContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(detalleContactoTelefonoLabel)
                                        .addComponent(detalleContactoTelefonoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(24, 24, 24)
                                .addGroup(detalleContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(detalleContactoCorreoLabel)
                                        .addComponent(detalleContactoCorreoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(detalleContactoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(detalleContactoDireccionLabel)
                                        .addComponent(detalleContactoDireccionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(55, Short.MAX_VALUE))
        );

        buscarOperadorPanel.setPreferredSize(new java.awt.Dimension(400, 300));

        buscarOperadorTituloLabel.setText("Operadores");

        buscarOperadorLabel.setText("Buscar:");

        buscarOperadorTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void removeUpdate(DocumentEvent e) {
                buscarOperadorTextFieldActionPerformed();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                buscarOperadorTextFieldActionPerformed();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });

        buscarOperadorOkButton.setText("Asignar");
        buscarOperadorOkButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscarOperadorOkButtonActionPerformed(evt);
            }
        });

        buscarOperadorValidacionLabel.setBackground(new java.awt.Color(255, 0, 0));
        buscarOperadorValidacionLabel.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        buscarOperadorValidacionLabel.setForeground(new java.awt.Color(255, 0, 0));
        buscarOperadorValidacionLabel.setText("");

        buscarOperadorList.setModel(new OperadorListModel(
                operadoresServicio.consultarOperadores(
                        null,
                        ObtenerOperador.builder().build()
                ).getBody()
        ));
        buscarOperadorList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        buscarOperadorList.setCellRenderer(new OperadorListRenderer());
        buscarOperadorList.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                operadorSeleccionado = buscarOperadorList.getSelectedValue();
            }
        });
        buscarOperadorjScrollPane.setViewportView(buscarOperadorList);

        javax.swing.GroupLayout buscarOperadorPanelLayout = new javax.swing.GroupLayout(buscarOperadorPanel);
        buscarOperadorPanel.setLayout(buscarOperadorPanelLayout);
        buscarOperadorPanelLayout.setHorizontalGroup(
                buscarOperadorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(buscarOperadorPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(buscarOperadorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(buscarOperadorPanelLayout.createSequentialGroup()
                                                .addComponent(buscarOperadorLabel)
                                                .addGap(18, 18, 18)
                                                .addComponent(buscarOperadorTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 332, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, buscarOperadorPanelLayout.createSequentialGroup()
                                                .addComponent(buscarOperadorValidacionLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(buscarOperadorOkButton))
                                        .addGroup(buscarOperadorPanelLayout.createSequentialGroup()
                                                .addComponent(buscarOperadorTituloLabel)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                        .addComponent(buscarOperadorjScrollPane))
                                .addContainerGap())
        );
        buscarOperadorPanelLayout.setVerticalGroup(
                buscarOperadorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(buscarOperadorPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(buscarOperadorTituloLabel)
                                .addGap(31, 31, 31)
                                .addGroup(buscarOperadorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(buscarOperadorLabel)
                                        .addComponent(buscarOperadorTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(buscarOperadorjScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(buscarOperadorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(buscarOperadorOkButton)
                                        .addComponent(buscarOperadorValidacionLabel))
                                .addContainerGap())
        );

        addTab(PedidoTab.CONSULTA.getTitulo(), consultaPanel);

    }

    private void consultaBuscarTextFieldActionPerformed() {
        consultarRegistros();
    }

    private void nuevoPedidoButtonActionPerformed(ActionEvent event) {
        nuevoPedidoButton.setEnabled(false);
        restablecerTablaSeleccion();

        nuevoEstadoJComboBox.setEditable(false);
        nuevoEstadoJComboBox.setEditable(false);

        enableTab(PedidoTab.CONSULTA.getTitulo(), false);
        enableTab(PedidoTab.DETALLE.getTitulo(), false);
        addTab(PedidoTab.REGISTRO.getTitulo(), registroPanel);
        setSelectedComponent(registroPanel);
    }

    private void detalleVerPedidoButtonActionPerformed(ActionEvent event) {
        detalleInProgress = true;
        pedidoTable.setRowSelectionAllowed(false);

        detallePedido = Objects.requireNonNull(pedidosIntegracionServicio.consultarPedidos(
                null,
                ObtenerPedido.builder()
                        .pedidoID((Integer) pedidoTable.getValueAt(pedidoTable.getSelectedRow(), 0))
                        .build()
        ).getBody()).get(0);

        fillDetallePedido();
        restablecerTablaSeleccion();
        enableTab(PedidoTab.CONSULTA.getTitulo(), false);
        enableTab(PedidoTab.REGISTRO.getTitulo(), false);
        addTab(PedidoTab.DETALLE.getTitulo(), detallePanel);
        setSelectedComponent(detallePanel);
    }

    private void detalleFillTablaProducto() {

        var productos = detallePedido.getProductos().stream()
                .map(p -> ObtenerProducto.builder()
                            .productoID(p.getProductoID())
                            .nombre(p.getNombre())
                            .cantidad(p.getCantidad())
                            .precioVenta(p.getPrecioVenta())
                            .fechaCaducidad(p.getFechaCaducidad())
                            .build()
                ).toList();

        detalleProductosjTable.setModel(new PedidoProductoTableModel(
                productos
        ));
    }

    private void consultaEstadojComboBoxActionPerformed(ActionEvent e) {
        consultarRegistros();
    }

    private void consultarRegistros() {

        var estadoSeleccionada = PedidoEstado.valorOf(
                Objects.requireNonNull(consultEstadoComboBox.getSelectedItem()).toString()
        );

        List<ConsultarPedido> registros = new ArrayList<>();

        var parametro = !consultaBuscarTextField.getText().trim().isEmpty()
                ? consultaBuscarTextField.getText()
                : null;
        switch (Objects.requireNonNull(estadoSeleccionada)) {
            case DEFECTO -> {
                registros = pedidosIntegracionServicio.consultarPedidos(
                        parametro,
                        ObtenerPedido.builder().build()
                ).getBody();
            }
            default -> {
                var obtenerPedido = ObtenerPedido.builder()
                        .estado(estadoSeleccionada.name())
                        .build();

                registros = pedidosIntegracionServicio.consultarPedidos(
                        parametro,
                        obtenerPedido
                ).getBody();
            }
        }

        pedidoTable.setModel(new PedidosTableModel(registros));
    }

    private void restablecerTablaSeleccion() {
        pedidoTable.getSelectionModel().clearSelection();
        selectedRegistroTable = null;
        detalleVerPedidoButton.setVisible(false);
    }

    private void nuevoContactoAgregarjButtonActionPerformed(ActionEvent event) {

        if(guardarContacto != null) {
            fillNuevoContactoRegistro();
        }

        JOptionPane nuevoContactoRegistrar = new JOptionPane();
        nuevoContactoRegistrarDialog = nuevoContactoRegistrar.createDialog(
                PedidoTab.REGISTRO_CONTACTO.getTitulo()
        );
        nuevoContactoRegistrarDialog.setSize(460, 430);
        nuevoContactoRegistrarDialog.setContentPane(nuevoContactoPanel);
        nuevoContactoRegistrarDialog.setVisible(true);
        nuevoContactoRegistrarDialog.addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(ComponentEvent e) {
            }

            @Override
            public void componentMoved(ComponentEvent e) {
            }

            @Override
            public void componentShown(ComponentEvent e) {
            }

            @Override
            public void componentHidden(ComponentEvent e) {
                nuevoContactoFinalizar();
            }

        });
    }

    private void nuevoContactoFinalizar() {
        nuevoContactoNombreTextField.setText("");
        nuevoContactoApellidoTextField.setText("");
        nuevoContactoTIpoDocTextField.setText("");
        nuevoContactoNumDocTextField.setText("");
        nuevoContactoTelefonoTextField.setText("");
        nuevoContactoCorreoTextField.setText("");
        nuevoContactoDireccionTextField.setText("");

        if (nuevoContactoRegistrarDialog != null) {
            nuevoContactoRegistrarDialog.setVisible(false);
        }
    }

    private void nuevoContactoRegistrarButtonActionPerformed(ActionEvent event) {
        if(validarNuevoContactoRegistro()) {
            registrarNuevoContacto();
        }
    }

    private boolean validarNuevoContactoRegistro() {

        boolean registroValido = true;

        if (nuevoContactoNombreTextField.getText().trim().isEmpty()) {
            nuevoContactoValidacionLabel.setText("El campo nombre es obligatorio.");
            registroValido = false;
        }

        if (nuevoContactoApellidoTextField.getText().trim().isEmpty()) {
            nuevoContactoValidacionLabel.setText("El campo apellido es obligatorio.");
            registroValido = false;
        }

        if (nuevoContactoTIpoDocTextField.getText().trim().isEmpty()) {
            nuevoContactoValidacionLabel.setText("El campo tipo documento es obligatorio.");
            registroValido = false;
        }

        if (nuevoContactoNumDocTextField.getText().trim().isEmpty()) {
            nuevoContactoValidacionLabel.setText("El campo numero documento es obligatorio.");
            registroValido = false;
        }

        if (nuevoContactoTelefonoTextField.getText().trim().isEmpty()) {
            nuevoContactoValidacionLabel.setText("El campo telefono es obligatorio.");
            registroValido = false;
        }

        if (nuevoContactoCorreoTextField.getText().trim().isEmpty()) {
            nuevoContactoValidacionLabel.setText("El campo correo es obligatorio.");
            registroValido = false;
        }

        if (nuevoContactoDireccionTextField.getText().trim().isEmpty()) {
            nuevoContactoValidacionLabel.setText("El campo direccion es obligatorio.");
            registroValido = false;
        }

        if (registroValido) {
            nuevoContactoValidacionLabel.setText("");
        }

        return registroValido;
    }

    private void registrarNuevoContacto() {

        guardarContacto = GuardarContacto.builder()
                .nombre(nuevoContactoNombreTextField.getText())
                .apellidos(nuevoContactoApellidoTextField.getText())
                .tipoDocumento(nuevoContactoTIpoDocTextField.getText())
                .numeroDocumento(nuevoContactoNumDocTextField.getText())
                .telefono(nuevoContactoTelefonoTextField.getText())
                .correo(nuevoContactoCorreoTextField.getText())
                .direccion(nuevoContactoDireccionTextField.getText())
                .build();

        nuevoContactojTextField.setText(
                String.format("%s, %s",
                        guardarContacto.getApellidos(),
                        guardarContacto.getNombre()
                )
        );
        nuevoContactoFinalizar();

    }

    private void fillNuevoContactoRegistro() {
        nuevoContactoNombreTextField.setText(guardarContacto.getNombre());
        nuevoContactoApellidoTextField.setText(guardarContacto.getApellidos());
        nuevoContactoTIpoDocTextField.setText(guardarContacto.getTipoDocumento());
        nuevoContactoNumDocTextField.setText(guardarContacto.getNumeroDocumento());
        nuevoContactoTelefonoTextField.setText(guardarContacto.getTelefono());
        nuevoContactoCorreoTextField.setText(guardarContacto.getCorreo());
        nuevoContactoDireccionTextField.setText(guardarContacto.getDireccion());
    }

    private void nuevoProductoAgregarjButtonActionPerformed(ActionEvent event) {

        seleccionProductoList.setModel(new ProductoListModel(
                productosServicio.consultarProductos(
                        null,
                        ObtenerProducto.builder().build()
                ).getBody()
        ));

        nuevoProductoTablaDeselecion();

        if(productoSeleccionado != null) {
            seleccionProductoList.setSelectedValue(productoSeleccionado, true);
        }

        JOptionPane seleccionProducto = new JOptionPane();
        seleccionProductoDialog = seleccionProducto.createDialog(
                PedidoTab.SELECCION_PRODUCTO.getTitulo()
        );
        seleccionProductoDialog.setSize(430, 350);
        seleccionProductoDialog.setContentPane(seleccionProductoPanel);
        seleccionProductoDialog.setVisible(true);
        seleccionProductoDialog.addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(ComponentEvent e) {
            }

            @Override
            public void componentMoved(ComponentEvent e) {
            }

            @Override
            public void componentShown(ComponentEvent e) {
            }

            @Override
            public void componentHidden(ComponentEvent e) {
                seleccionProductoFinalizar();
            }

        });

    }

    private void nuevoProductoTablaDeselecion() {
        nuevoProductosjTable.getSelectionModel().clearSelection();
        nuevoSelectedProductoTable = null;
        nuevoProductoEliminarjButton.setEnabled(false);
    }

    private void seleccionProductoFinalizar() {
        seleccionBuscarProductoTextField.setText("");
        seleccionCantidadTextField.setText("");

        seleccionProductoList.setModel(new ProductoListModel(
                new ArrayList<>()
        ));

        productoSeleccionado = ObtenerProducto.builder().build();

        seleccionProductoDialog.setVisible(false);
    }

    private void seleccionBuscarProductoTextFieldActionPerformed() {

        var productos = productosServicio.consultarProductos(
                seleccionBuscarProductoTextField.getText(),
                ObtenerProducto.builder().build()
        ).getBody();

        if (!productos.isEmpty()) {
            seleccionProductoList.setModel(new ProductoListModel(productos));

            if (productoSeleccionado != null) {
                productos.stream()
                        .filter(p -> Objects.equals(
                                p.getProductoID(),
                                productoSeleccionado.getProductoID()
                        ))
                        .findFirst()
                        .ifPresent(p -> {
                            seleccionProductoList.setSelectedValue(p, true);
                        });
            }

        } else {
            seleccionProductoList.setModel(new ProductoListModel(new ArrayList<>()));
        }
    }

    private void seleccionProductoOkButtonActionPerformed(ActionEvent event) {
        if(validarSeleccionProducto()) {
            seleccionProductoRegistrar();
        }
    }

    private boolean validarSeleccionProducto() {
        boolean registroValido = true;

        if (productoSeleccionado.getProductoID() == null) {
            seleccionProductoValidacionLabel.setText("Seleccione un producto.");
            registroValido = false;
        }

        if (seleccionCantidadTextField.getText().trim().isEmpty()) {
            seleccionProductoValidacionLabel.setText("El campo cantidad es obligatorio.");
            registroValido = false;
        }

        try {
            var cantidad = formatDouble.apply(seleccionCantidadTextField.getText());

            var prodducto = productosServicio.verificarStockProducto(
                    productoSeleccionado.getProductoID(),
                    cantidad
            ).getBody();

            if(Objects.requireNonNull(prodducto).getProductoID() == null) {
                throw new Exception("ProductoNoDisponibleException");
            }

        } catch (NumberFormatException e) {
            seleccionProductoValidacionLabel.setText("La cantidad no es valida.");
            registroValido = false;
        } catch (Exception e) {
            seleccionProductoValidacionLabel.setText("El producto no tiene stock.");
            registroValido = false;
        }


        if (registroValido) {
            seleccionProductoValidacionLabel.setText("");
        }

        return registroValido;
    }

    private void seleccionProductoRegistrar() {

        var existeProduct = nuevoPedidosProductos.stream()
                .filter(pc -> compararProductos(pc, productoSeleccionado))
                .findFirst();

        if(existeProduct.isPresent()){

            var productoExistente = existeProduct.get();

            var i = nuevoPedidosProductos.indexOf(productoExistente);

            productoExistente.setCantidad(
                    productoExistente.getCantidad() + formatDouble.apply(seleccionCantidadTextField.getText())
            );
            nuevoPedidosProductos.set(
                    i, productoExistente
            );

        } else {
            productoSeleccionado.setCantidad(
                    formatDouble.apply(seleccionCantidadTextField.getText())
            );
            nuevoPedidosProductos.add(productoSeleccionado);
        }

        nuevoProductosjTable.setModel(new PedidoProductoTableModel(
                nuevoPedidosProductos
        ));

        seleccionProductoFinalizar();
    }

    private boolean compararProductos(ObtenerProducto o, ObtenerProducto that) {
            return Objects.equals(o.getProductoID(), that.getProductoID()) &&
                    Double.compare(that.getPrecioCompra(), o.getPrecioCompra()) == 0 &&
                    Double.compare(that.getPrecioVenta(), o.getPrecioVenta()) == 0 &&
                    o.getFechaCaducidad().equals(that.getFechaCaducidad());
    }

    private void nuevoProductoTablaSeleccionMouseClicked() {
        int row = nuevoProductosjTable.getSelectedRow();

        if (nuevoSelectedProductoTable != null && nuevoSelectedProductoTable.equals(row)) {
            nuevoProductoTablaDeselecion();
        } else {
            nuevoProductoEliminarjButton.setEnabled(true);
            nuevoSelectedProductoTable = row;
        }
    }

    private void nuevoProductoEliminarjButtonActionPerformed(ActionEvent event) {
        nuevoPedidosProductos.remove(nuevoSelectedProductoTable.intValue());

        nuevoProductoTablaDeselecion();

        nuevoProductosjTable.setModel(new PedidoProductoTableModel(
                nuevoPedidosProductos
        ));
    }

    private void nuevoRegistrarButtonActionPerformed(ActionEvent event) {
        if (validarRegistro()) {
            registrarPedido();
        }
    }

    private void nuevoCancelarButtonActionPerformed(ActionEvent event) {

        String[] opciones = {"Sí", "No"};

        int registrar = JOptionPane.showOptionDialog(
                this,
                "Se cancelara el registro.",
                PedidoTab.REGISTRO.getTitulo(),
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                opciones,
                opciones[1]);

        if (registrar == JOptionPane.YES_OPTION) {
            nuevoRegistroFinalizar();
        }
    }

    private boolean validarRegistro() {

        boolean registroValido = true;

        if (nuevoNumeroVentaTextField.getText().trim().isEmpty()) {
            nuevoValidacionLabel.setText("El campo número de venta es obligatorio.");
            registroValido = false;
        }
        if (nuevoFechaEntregaTextField.getText().trim().isEmpty()) {
            nuevoValidacionLabel.setText("El campo fecha entrega es obligatorio.");
            registroValido = false;
        }
        if (nuevoContactojTextField.getText().trim().isEmpty()) {
            nuevoValidacionLabel.setText("El campo contacto es obligatorio.");
            registroValido = false;
        }

        if (nuevoPedidosProductos.isEmpty()) {
            nuevoValidacionLabel.setText("Debe agregar productos de compra.");
            registroValido = false;
        }

        try {
            Integer.parseInt(
                    nuevoNumeroVentaTextField.getText()
            );
        } catch (NumberFormatException e) {
            nuevoValidacionLabel.setText("El numero de venta no es valido.");
            registroValido = false;
        }

        try {
            LocalDate.parse(
                    nuevoFechaEntregaTextField.getText()
            );
        } catch (DateTimeParseException e) {
            nuevoValidacionLabel.setText("El fecha entrega no es valido.");
            registroValido = false;
        }

        if (registroValido) {
            nuevoValidacionLabel.setText("");
        }

        return registroValido;
    }

    private void registrarPedido() {

        String[] opciones = {"Sí", "No"};

        int registrar = JOptionPane.showOptionDialog(
                this,
                "Se registrará un nuevo pedido.",
                PedidoTab.REGISTRO.getTitulo(),
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                opciones,
                opciones[1]);

        if (registrar == JOptionPane.YES_OPTION) {

            try {

                var nuevoPedido = GuardarPedido.builder()
                        .ventaID(Integer.parseInt(
                                nuevoNumeroVentaTextField.getText()
                        ))
                        .fechaEntrega(
                                LocalDate.parse(
                                        nuevoFechaEntregaTextField.getText()
                                )
                        )
                        .estado(PedidoEstado.PENDIENTE.name())
                        .observaciones("")
                        .fechaTransaccion(OffsetDateTime.now())
                        .build();

                var pedidoProductos = nuevoPedidosProductos.stream()
                        .map(p ->  GuardarPedidoProducto.builder()
                                    .productoID(p.getProductoID())
                                    .cantidad(p.getCantidad())
                                    .build()
                        ).toList();

                var nuevoRegistroPedido = RegistrarPedido.builder()
                        .pedido(nuevoPedido)
                        .contacto(guardarContacto)
                        .productos(pedidoProductos)
                        .build();

                nuevoRegistroPedido = pedidosIntegracionServicio.registrarPedido(nuevoRegistroPedido)
                        .getBody();

                if (nuevoRegistroPedido.getPedido().getPedidoID() != null) {
                    System.out.println("Se registro nuevo pedido: " + nuevoRegistroPedido.getPedido().getPedidoID());
                }

                nuevoRegistroFinalizar();

            } catch (Exception e) {
                nuevoValidacionLabel.setText(e.getMessage());
                nuevoValidacionLabel.setVisible(true);
            }

        }
    }

    private void nuevoRegistroFinalizar() {

        nuevoNumeroVentaTextField.setText("");
        nuevoFechaEntregaTextField.setText(
                formatLocalDateToString.apply(LocalDate.now())
        );

        nuevoPedidosProductos = new ArrayList<>();
        nuevoContactojTextField.setText("");

        guardarContacto = null;

        nuevoProductosjTable.setModel(new PedidoProductoTableModel(
                nuevoPedidosProductos
        ));


        pedidoTable.setModel(new PedidosTableModel(
                pedidosIntegracionServicio.consultarPedidos(
                        null,
                        ObtenerPedido.builder().build()
                ).getBody()
        ));
        pedidoTable.setRowSelectionAllowed(true);


        nuevoProductoTablaDeselecion();

        nuevoContactoFinalizar();

        nuevoPedidoButton.setEnabled(true);
        enableTab(PedidoTab.CONSULTA.getTitulo(), true);
        enableTab(PedidoTab.DETALLE.getTitulo(), true);
        removeTab(PedidoTab.REGISTRO.getTitulo());
        setSelectedComponent(consultaPanel);

        consultaBuscarTextFieldActionPerformed();

    }

    private void fillDetallePedido() {
        detallePedidoLabel.setText("Detalle Pedido : N° " + detallePedido.getPedido().getPedidoID());

        detalleNumeroVentaTextField.setText(
                detallePedido.getPedido().getVentaID().toString()
        );
        detalleNumeroVentaTextField.setEditable(false);

        detalleFechaEntregaTextField.setText(
                detallePedido.getPedido().getFechaEntrega().toString()
        );
        detalleFechaEntregaTextField.setEditable(false);

        detalleEstadoComboBox.setSelectedItem(
                PedidoEstado.valueOf(
                        detallePedido.getPedido().getEstado()
                ).getDescripcion()
        );
        detalleEstadoComboBox.setEditable(false);
        detalleEstadoComboBox.setEnabled(false);

        detalleContactoTextField.setEditable(false);
        detalleContactoTextField.setText(
                String.format("%s, %s",
                        detallePedido.getContacto().getApellidos(),
                        detallePedido.getContacto().getNombre()
                )
        );

        detalleOperadorTextField.setEditable(false);

        if (detallePedido.getOperador().getOperadorID() != null) {
            detalleOperadorTextField.setText(
                    String.format("%s | %s, %s",
                            detallePedido.getOperador().getOperadorID(),
                            detallePedido.getOperador().getApellidos(),
                            detallePedido.getOperador().getNombre()
                    )
            );
        }

        if (detallePedido.getPedido().getEstado().equals(PedidoEstado.EN_ATENCION.name())) {
            detalleAtenderButton.setVisible(true);
        }

        if (detallePedido.getPedido().getEstado().equals(PedidoEstado.ATENDIDO.name())) {
            detalleBuscarOperadorjButton.setEnabled(false);
        } else {
            detalleBuscarOperadorjButton.setEnabled(true);
        }

        detalleFillTablaProducto();
    }

    private void detalleVerContactojButtonActionPerformed(ActionEvent event) {

        fillDetalleContacto();

        JOptionPane detalleContacto = new JOptionPane();
        detalleContactoDialog = detalleContacto.createDialog(
                PedidoTab.DETALLE_CONTACTO.getTitulo()
        );
        detalleContactoDialog.setSize(460, 430);
        detalleContactoDialog.setContentPane(detalleContactoPanel);
        detalleContactoDialog.setVisible(true);
        detalleContactoDialog.addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(ComponentEvent e) {
            }

            @Override
            public void componentMoved(ComponentEvent e) {
            }

            @Override
            public void componentShown(ComponentEvent e) {
            }

            @Override
            public void componentHidden(ComponentEvent e) {
                detalleContactoFinalizar();
            }

        });

    }

    private void fillDetalleContacto() {
        var contacto = detallePedido.getContacto();

        detalleContactoNombreTextField.setText(contacto.getNombre());
        detalleContactoNombreTextField.setEditable(false);

        detalleContactoApellidoTextField.setText(contacto.getApellidos());
        detalleContactoApellidoTextField.setEditable(false);

        detalleContactoTIpoDocTextField.setText(contacto.getTipoDocumento());
        detalleContactoTIpoDocTextField.setEditable(false);

        detalleContactoNumDocTextField.setText(contacto.getNumeroDocumento());
        detalleContactoNumDocTextField.setEditable(false);

        detalleContactoTelefonoTextField.setText(contacto.getTelefono());
        detalleContactoTelefonoTextField.setEditable(false);

        detalleContactoCorreoTextField.setText(contacto.getCorreo());
        detalleContactoCorreoTextField.setEditable(false);

        detalleContactoDireccionTextField.setText(contacto.getDireccion());
        detalleContactoDireccionTextField.setEditable(false);

    }

    private void detalleContactoFinalizar() {
        detalleContactoNombreTextField.setText("");
        detalleContactoApellidoTextField.setText("");
        detalleContactoTIpoDocTextField.setText("");
        detalleContactoNumDocTextField.setText("");
        detalleContactoTelefonoTextField.setText("");
        detalleContactoCorreoTextField.setText("");
        detalleContactoDireccionTextField.setText("");

        if (detalleContactoDialog != null) {
            detalleContactoDialog.setVisible(false);
        }
    }

    private void detalleBuscarOperadorjButtonActionPerformed(ActionEvent event) {

        if (detallePedido.getOperador().getOperadorID() != null) {
            operadorSeleccionado = detallePedido.getOperador();
        }

        buscarOperadorTextFieldActionPerformed();

        JOptionPane detalleOperador = new JOptionPane();
        detalleOperadorDialog = detalleOperador.createDialog(
                PedidoTab.ASIGNAR_OPERADOR.getTitulo()
        );
        detalleOperadorDialog.setSize(460, 350);
        detalleOperadorDialog.setContentPane(buscarOperadorPanel);
        detalleOperadorDialog.setVisible(true);
        detalleOperadorDialog.addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(ComponentEvent e) {
            }

            @Override
            public void componentMoved(ComponentEvent e) {
            }

            @Override
            public void componentShown(ComponentEvent e) {
            }

            @Override
            public void componentHidden(ComponentEvent e) {
                detalleOperadorFinalizar();
            }

        });

    }

    private void detalleOperadorFinalizar() {

        buscarOperadorTextField.setText("");

        buscarOperadorList.setModel(new OperadorListModel(
                new ArrayList<>()
        ));

        operadorSeleccionado = ObtenerOperador.builder().build();

        if (detalleOperadorDialog != null) {
            detalleOperadorDialog.setVisible(false);
        }

        detallePedido = Objects.requireNonNull(pedidosIntegracionServicio.consultarPedidos(
                null,
                ObtenerPedido.builder()
                        .pedidoID(detallePedido.getPedido().getPedidoID())
                        .build()
        ).getBody()).get(0);

        fillDetallePedido();
    }

    private void buscarOperadorTextFieldActionPerformed() {
        var operadores = operadoresServicio.consultarOperadores(
                buscarOperadorTextField.getText(),
                ObtenerOperador.builder().build()
        ).getBody();

        if (!operadores.isEmpty()) {
            buscarOperadorList.setModel(new OperadorListModel(operadores));

            if (operadorSeleccionado != null) {
                operadores.stream()
                        .filter(p -> Objects.equals(
                                p.getOperadorID(),
                                operadorSeleccionado.getOperadorID()
                        ))
                        .findFirst()
                        .ifPresent(p -> {
                            seleccionProductoList.setSelectedValue(p, true);
                        });
            }

        } else {
            buscarOperadorList.setModel(new OperadorListModel(new ArrayList<>()));
        }
    }

    private void buscarOperadorOkButtonActionPerformed(ActionEvent event) {
        if (validarOperador()) {
            asginarOperador();
        }
    }

    private boolean validarOperador() {
        boolean registroValido = true;

        if (operadorSeleccionado.getOperadorID() == null) {
            seleccionProductoValidacionLabel.setText("Seleccione un operador.");
            registroValido = false;
        }


        if (registroValido) {
            buscarOperadorValidacionLabel.setText("");
        }

        return registroValido;
    }

    private void asginarOperador () {

        String[] opciones = {"Sí", "No"};

        int registrar = JOptionPane.showOptionDialog(
                this,
                "Se registrará un operador al pedido, y pasará a en atención.",
                PedidoTab.ASIGNAR_OPERADOR.getTitulo(),
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                opciones,
                opciones[1]);

        if (registrar == JOptionPane.YES_OPTION) {

            try {
                var pedido = ObtenerPedido.builder()
                        .pedidoID(detallePedido.getPedido().getPedidoID())
                        .estado(PedidoEstado.EN_ATENCION.name())
                        .build();

                var asignarPedido = AsignarPedido.builder()
                        .pedido(pedido)
                        .operador(operadorSeleccionado)
                        .build();

                asignarPedido = pedidosIntegracionServicio.asignarPedido(
                        pedido.getPedidoID(),
                        asignarPedido
                ).getBody();

                if (asignarPedido.getPedido().getPedidoID() != null) {
                    System.out.println("se asigno correctamente el pedido: " + asignarPedido.getPedido().getPedidoID() );
                }

                detalleOperadorFinalizar();

            } catch (Exception e) {
                buscarOperadorValidacionLabel.setText(e.getMessage());
            }

        }
    }

    private void detalleAtenderButtonActionPerformed(ActionEvent event) {

        String[] opciones = {"Sí", "No"};

        int registrar = JOptionPane.showOptionDialog(
                this,
                "Se atenderá el pedido.",
                PedidoTab.ATENDER_PEDIDO.getTitulo(),
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                opciones,
                opciones[1]);

        if (registrar == JOptionPane.YES_OPTION) {
                try {

                    var pedidoAtendido = pedidosIntegracionServicio.atenderPedido(
                            detallePedido.getPedido().getPedidoID()
                    ).getBody();

                    if (pedidoAtendido.getPedido().getPedidoID() != null) {
                        System.out.println("se atendio correctamente el pedido: " + pedidoAtendido.getPedido().getPedidoID() );
                    }

                    detalleAtenderinalizar();
                } catch (Exception e) {
                    detalleValidacionLabel.setText(e.getMessage());
                }

        }

    }

    private void detalleAtenderinalizar() {

        detalleAtenderButton.setVisible(false);

        detallePedido = Objects.requireNonNull(pedidosIntegracionServicio.consultarPedidos(
                null,
                ObtenerPedido.builder()
                        .pedidoID(detallePedido.getPedido().getPedidoID())
                        .build()
        ).getBody()).get(0);

        fillDetallePedido();
    }

    private void detalleSalirButtonActionPerformed(ActionEvent event) {

        detallePedidoLabel.setText("");
        detalleNumeroVentaTextField.setText("");
        detalleEstadoComboBox.setSelectedItem(PedidoEstado.DEFECTO.name());

        detalleContactoTextField.setText("");
        detalleOperadorTextField.setText("");

        detalleAtenderButton.setVisible(false);

        detalleProductosjTable.setModel(new PedidoProductoTableModel(new ArrayList<>()));


        pedidoTable.setRowSelectionAllowed(true);
        detalleInProgress = false;
        nuevoPedidoButton.setEnabled(true);

        enableTab(PedidoTab.CONSULTA.getTitulo(), true);
        enableTab(PedidoTab.REGISTRO.getTitulo(), true);
        removeTab(PedidoTab.DETALLE.getTitulo());
        setSelectedComponent(consultaPanel);

        consultaBuscarTextFieldActionPerformed();

    }

    private void enableTab(String tabTitle, boolean enabled) {
        for (int i = 0; i < getTabCount(); i++) {
            if (getTitleAt(i).equals(tabTitle)) {
                setEnabledAt(0, enabled);
            }
        }
    }

    private void removeTab(String tabTitle) {
        for (int i = 0; i < getTabCount(); i++) {
            if (getTitleAt(i).equals(tabTitle)) {
                remove(i);
                break;
            }
        }
    }

    public enum PedidoTab {
        CONSULTA("Consulta de pedido"),
        REGISTRO("Registro de pedido"),
        DETALLE("Detalle de pedido"),
        REGISTRO_CONTACTO("Registro de contacto"),
        DETALLE_CONTACTO("Detalle de contacto"),
        SELECCION_PRODUCTO("Seleccion de producto"),
        ASIGNAR_OPERADOR("Asignar operador"),
        ATENDER_PEDIDO("Atender Pedido");

        private final String titulo;

        PedidoTab(String titulo) {
            this.titulo = titulo;
        }

        public String getTitulo() {
            return titulo;
        }


    }

    public enum PedidoEstado {
        DEFECTO(""),
        PENDIENTE("Pendiente"),
        EN_ATENCION("En atencion"),
        ATENDIDO("Atendido");

        private final String descripcion;

        PedidoEstado(String descripcion) {
            this.descripcion = descripcion;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public static PedidoEstado valorOf(String value) {
            return Arrays.stream(values())
                    .filter(x->x.getDescripcion().equals(value))
                    .findFirst()
                    .orElse(PedidoEstado.DEFECTO);
        }
    }

    private class PedidosTableModel extends AbstractTableModel {

        private final String[] columnNames
                = new String[]{
                "N° Pedido", "N° Venta", "Estado", "Contacto", "Fecha Entrega"
        };

        private final java.util.List<ConsultarPedido> registros;

        public PedidosTableModel() {
            registros = new ArrayList<>();
        }

        public PedidosTableModel(List<ConsultarPedido> regisrtos) {
            this.registros = regisrtos;
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        @Override
        public int getRowCount() {
            return registros.size();
        }

        @Override
        public Object getValueAt(int row, int column) {
            var registro = getRegistro(row);

            return switch (column) {
                case 0 -> registro.getPedido().getPedidoID();
                case 1 -> registro.getPedido().getVentaID();
                case 2 -> registro.getPedido().getEstado();
                case 3 -> String.format(
                        "%s , %s",
                        registro.getContacto().getApellidos(),
                        registro.getContacto().getNombre()
                );
                case 4 -> registro.getPedido().getFechaEntrega().toString();
                default -> null;
            };
        }

        @Override
        public void setValueAt(Object value, int row, int column) {
            var registro = getRegistro(row);

            switch (column) {
                case 0 -> registro.getPedido().setPedidoID((Integer) value);
                case 1 -> registro.getPedido().setVentaID((Integer) value);
                case 2 -> registro.getPedido().setEstado((String) value);
                case 3 -> registro.getContacto().setCorreo((String) value);
                case 4 -> registro.getPedido().setFechaEntrega((LocalDate) value);
            }

            fireTableCellUpdated(row, column);
        }

        public ConsultarPedido getRegistro(int row) {
            return registros.get(row);
        }


    }

    private class PedidoProductoTableModel extends AbstractTableModel {

        private final String[] columnNames = new String[]{
                "ID Producto",
                "Nombre",
                "Cantidad",
                "Precio Venta",
                "Fecha Caducidad"
        };

        private final List<ObtenerProducto> pedidoProductoList;

        public PedidoProductoTableModel() {
            pedidoProductoList = new ArrayList<>();
        }

        public PedidoProductoTableModel(List<ObtenerProducto> pedidoProductoList) {
            this.pedidoProductoList = pedidoProductoList;
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        @Override
        public int getRowCount() {
            return pedidoProductoList.size();
        }

        @Override
        public Object getValueAt(int row, int column) {
            var producto = getPedidoProducto(row);

            return switch (column) {
                case 0 -> producto.getProductoID();
                case 1 -> producto.getNombre();
                case 2 -> producto.getCantidad();
                case 3 -> producto.getPrecioVenta();
                case 4 -> formatOffsetDateTimaToString.apply(producto.getFechaCaducidad());
                default -> null;
            };
        }

        @Override
        public void setValueAt(Object value, int row, int column) {
            var producto = getPedidoProducto(row);

            switch (column) {
                case 0 -> producto.setProductoID((Integer) value);
                case 1 -> producto.setNombre((String) value);
                case 2 -> producto.setCantidad((Double) value);
                case 3 -> producto.setPrecioVenta((Double) value);
                case 4 -> producto.setFechaCaducidad((OffsetDateTime) value);
            }

            fireTableCellUpdated(row, column);
        }

        public ObtenerProducto getPedidoProducto(int row) {
            return pedidoProductoList.get(row);
        }

    }

    public class ProductoListModel extends AbstractListModel<ObtenerProducto> {
        private List<ObtenerProducto> productos;

        public ProductoListModel(List<ObtenerProducto> productos) {
            this.productos = productos;
        }

        public void addProducto(ObtenerProducto producto) {
            this.productos.add(producto);
            int index = this.productos.size() - 1;
            fireIntervalAdded(this, index, index);
        }

        @Override
        public int getSize() {
            return productos.size();
        }

        @Override
        public ObtenerProducto getElementAt(int index) {
            return productos.get(index);
        }
    }

    public class ProductoListRenderer extends DefaultListCellRenderer {
        @Override
        public java.awt.Component getListCellRendererComponent(
                JList<?> list,
                Object value,
                int index,
                boolean isSelected,
                boolean cellHasFocus
        ) {
            if (value instanceof ObtenerProducto producto) {
                value = String.format("%s | %s",
                        producto.getProductoID(),
                        producto.getNombre());
            }
            return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        }
    }

    public class OperadorListModel extends AbstractListModel<ObtenerOperador> {
        private List<ObtenerOperador> operadores;

        public OperadorListModel(List<ObtenerOperador> operadores) {
            this.operadores = operadores;
        }

        public void addOperador(ObtenerOperador operador) {
            this.operadores.add(operador);
            int index = this.operadores.size() - 1;
            fireIntervalAdded(this, index, index);
        }

        @Override
        public int getSize() {
            return operadores.size();
        }

        @Override
        public ObtenerOperador getElementAt(int index) {
            return operadores.get(index);
        }
    }

    public class OperadorListRenderer extends DefaultListCellRenderer {
        @Override
        public java.awt.Component getListCellRendererComponent(
                JList<?> list,
                Object value,
                int index,
                boolean isSelected,
                boolean cellHasFocus
        ) {
            if (value instanceof ObtenerOperador operador) {
                value = String.format("%s | %s, %s",
                        operador.getOperadorID(),
                        operador.getApellidos(),
                        operador.getNombre());
            }
            return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        }
    }


}
