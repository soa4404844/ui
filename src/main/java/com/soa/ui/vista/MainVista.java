package com.soa.ui.vista;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;

@Component
public class MainVista extends JFrame {

    @Autowired
    private PedidosVista pedidosVista;

    private JPanel principalPanel;
    private JPanel componentePanel;
    private JToolBar principalToolBar;

    private JButton pedidosButton;
    private JButton salirButton;

    public void initComponents() {

        pedidosVista.initComponents();


        principalPanel = new javax.swing.JPanel();

        componentePanel = new javax.swing.JPanel();

        principalToolBar = new javax.swing.JToolBar();

        pedidosButton = new javax.swing.JButton();

        salirButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        principalPanel.setPreferredSize(new java.awt.Dimension(980, 640));

        componentePanel.setMinimumSize(new java.awt.Dimension(880, 640));
        componentePanel.setPreferredSize(new java.awt.Dimension(880, 640));

        principalToolBar.setFloatable(false);
        principalToolBar.setOrientation(javax.swing.SwingConstants.VERTICAL);
        principalToolBar.setRollover(true);
        principalToolBar.setToolTipText("");
        principalToolBar.setMaximumSize(new java.awt.Dimension(100, 640));
        principalToolBar.setMinimumSize(new java.awt.Dimension(100, 640));
        principalToolBar.setPreferredSize(new java.awt.Dimension(100, 640));

        pedidosButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/compra.png"))); // NOI18N
        pedidosButton.setText("Pedidos");
        pedidosButton.setToolTipText("Pedidos");
        pedidosButton.setFocusable(false);
        pedidosButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        pedidosButton.setMaximumSize(new java.awt.Dimension(99, 90));
        pedidosButton.setMinimumSize(new java.awt.Dimension(99, 90));
        pedidosButton.setPreferredSize(new java.awt.Dimension(99, 90));
        pedidosButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        pedidosButton.addActionListener((java.awt.event.ActionEvent evt) -> {
            menuButtonActionPerformed(evt, Menu.PEDIDOS);
        });

        salirButton.setText("Salir");
        salirButton.setToolTipText("Salir");
        salirButton.setFocusable(false);
        salirButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        salirButton.setMaximumSize(new java.awt.Dimension(99, 20));
        salirButton.setMinimumSize(new java.awt.Dimension(99, 20));
        salirButton.setPreferredSize(new java.awt.Dimension(99, 30));
        salirButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        salirButton.addActionListener((java.awt.event.ActionEvent evt) -> {
            menuButtonActionPerformed(evt, Menu.SALIR);
        });


        principalToolBar.add(pedidosButton);
        principalToolBar.add(new Box.Filler(
                new Dimension(0, 0),
                new Dimension(0, 0),
                new Dimension(0, 32767)
        ));
        principalToolBar.add(salirButton);

        javax.swing.GroupLayout componentePanel1Layout = new javax.swing.GroupLayout(componentePanel);
        componentePanel.setLayout(componentePanel1Layout);
        componentePanel1Layout.setHorizontalGroup(
                componentePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(pedidosVista, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(0, 880, Short.MAX_VALUE)
        );
        componentePanel1Layout.setVerticalGroup(
                componentePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(pedidosVista, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout principalPanelLayout = new javax.swing.GroupLayout(principalPanel);
        principalPanel.setLayout(principalPanelLayout);
        principalPanelLayout.setHorizontalGroup(
                principalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(principalPanelLayout.createSequentialGroup()
                                .addComponent(principalToolBar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(componentePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 838, Short.MAX_VALUE))
        );
        principalPanelLayout.setVerticalGroup(
                principalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(componentePanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 652, Short.MAX_VALUE)
                        .addComponent(principalToolBar, javax.swing.GroupLayout.DEFAULT_SIZE, 652, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(principalPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 944, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(principalPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 652, Short.MAX_VALUE)
        );

        pack();


    }

    private void loginVistaPropertyChageEventt(PropertyChangeEvent event) {

    }

    private void initMenuComponents() {

        principalPanel.setVisible(true);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(principalPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 840, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(principalPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 480, Short.MAX_VALUE)
        );

    }


    private void menuButtonActionPerformed(ActionEvent event, Menu vista) {
        switch (vista) {
            case PEDIDOS -> pedidosVista.setVisible(true);
            case SALIR -> salirButtonActionPerformed();
            default -> {
            }
        }
    }

    private void salirButtonActionPerformed() {
    }

    public enum Menu {
        PEDIDOS,
        SALIR
    }
}
